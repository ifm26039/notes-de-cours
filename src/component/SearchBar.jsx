import React, { useRef } from 'react';
import { useHistory } from 'react-router-dom';
import useOnClickOutside from './useOnClickOutside';

import styles from './SearchBar.module.css'

export default function SearchBar(props) {
    const searchRef = useRef(null);
    const submitRef = useRef(null);
    const buttonRef = useRef(null);
    const history = useHistory();
    let inTransition = false;

    const focusInput = () => {
        searchRef.current.removeEventListener("transitionend", focusInput);
        inTransition = false;
        searchRef.current.focus();
    }

    const toggleSearchBar = () => {
        if(inTransition || window.innerWidth < 640){
            return;
        }

        if(!props.visible){
            inTransition = true;
            searchRef.current.addEventListener("transitionend", focusInput);
        }
        
        props.onToggleSearchBar();
    }

    const closeSearchBar = () => {
        if(props.visible){
            toggleSearchBar();
        }
    }

    const submitSearch = (event) => {
        event.preventDefault();
        if(searchRef.current.value.length > 0){
            history.push(`/search?value=${ encodeURI(searchRef.current.value) }`);
            searchRef.current.value = '';
            closeSearchBar();
            if(props.onSearch){
                props.onSearch();
            }
        }
    }

    useOnClickOutside([buttonRef, searchRef, submitRef], closeSearchBar);

    return <form className={ styles.search } onSubmit={ submitSearch }>
        <button type="button" ref={ buttonRef } onClick={ toggleSearchBar }>
            <i className="material-icons">search</i>
        </button>
        <input type="search" ref={ searchRef } className={ (props.visible ? styles.visible : "") } />
        <input type="submit" ref={ submitRef } className={ (props.visible ? styles.visible : "") } value="Recherche" />
    </form>
}
