import React from 'react';

import styles from './Logo.module.css'

export default function Logo() {
    return <div className={ styles.logo }>
        <div className={ styles.title }>
            <div>Programmation web</div>
            <span>avancé</span>
        </div>
    </div>
}
