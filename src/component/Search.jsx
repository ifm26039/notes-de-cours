import React, { useEffect, useState } from 'react'
import { Link, useHistory, useLocation } from 'react-router-dom';
import queryString from 'qs'
import SearchEngine from '../searchEngine'

import styles from './Search.module.css'
import { Helmet } from 'react-helmet-async';

export default function Search() {
    const [searchResult, setSearchResult] = useState([]);
    const location = useLocation();
    const history = useHistory();

    const searchString = queryString.parse(location.search).value;
    if(!searchString){
        history.push('/');
    }

    useEffect(() => {
        const searchEngine = new SearchEngine();
        let results = searchEngine.search(searchString);
        setSearchResult(results);
    }, [searchString]);

    return <>
        <Helmet>
            <title>Recherche</title>
            <meta name="description" content={ `Résultat de recherche dans ce site.` } />
        </Helmet>

        <section className={ styles.result }>
            <h1>Résultats de recherche</h1>
            <ul>
                { searchResult.map((result, i) => (
                    <li key={ i }>
                        { result.page.component ? 
                            <Link to={ `/${ result.sectionId }/${ result.page.id }` }>{ result.page.title }</Link> :
                            <a href={ result.page.url } target="_blank" rel="noopener noreferrer">{ result.page.title }</a>
                        }
                    </li>
                )) }
            </ul>
        </section>
    </>
}
