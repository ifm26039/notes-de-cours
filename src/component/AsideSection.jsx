import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import useCurrentPage from './useCurrentPage';

import styles from './AsideSection.module.css';

export default function AsideSection(props) {
    const [currentPage, currentSection] = useCurrentPage();

    const currentPageIsInGroup = (group) => {
        return currentPage && group.id === currentPage.group.id;
    }

    return <>
        { !props.disabled ?
            <Link to={ `/${ props.id }/` } onClick={ props.closeAside }>
                { props.completeName }
            </Link>
            :
            <span className={ styles.disabled }>{ props.completeName }</span>
        }
        
        { currentSection && props.id === currentSection.id && (
            <ul className={ styles.sublist }>
                { Object.values(props.groups).map((group) => (
                    <li key={ group.id }>
                        <span className={ currentPageIsInGroup(group) ? styles.active : '' }>{ group.label }</span>
                        <ul className={ styles.sublist }>
                            { group.pages.map((page, i) => (
                                <li key={ i }>
                                    { page.component ? 
                                        <NavLink to={ `/${ props.id }/${ page.id }` } onClick={ props.closeAside } className={ styles['page-link'] } activeClassName={ styles.active }>
                                            { page.title }
                                        </NavLink> :
                                        <a href={ page.url } target="_blank" rel="noopener noreferrer" className={ styles['page-link'] }>{ page.title }</a>
                                    }
                                </li>
                            )) }
                        </ul>
                    </li>
                )) }
            </ul>
        ) }
    </>
}
