import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { debounce } from "../Utils";

export default function useCheckOverflow(ratio = 1) {
    /**
     * State variable indicating whether the page content is overflowing over 
     * a certain ratio of the screen or not.
     */
    const [overflow, setOverflow] = useState(false);

    /**
     * Navigation history object.
     */
    const history = useHistory();

    /**
     * Check if the is an overflow by comparing the height of the window and 
     * the height of the page. This function is debounced, so it can be called 
     * multiple times without any problems.
     */
    const checkOverflow = debounce(() => {
        if(overflow !== (window.innerHeight * ratio < document.body.offsetHeight)){
            setOverflow((oldOverflow) => !oldOverflow);
        }
    }, 500);

    // Check if there is an overflow after the first render.
    useEffect(() => {
        checkOverflow();
    }, [checkOverflow]);

    // Check if there is an overflow after each resize of the window.
    useEffect(() => {
        window.addEventListener('resize', checkOverflow);
        return () => {
            window.removeEventListener('resize', checkOverflow);
        }
    }, [checkOverflow]);

    // Check if there is an overflow after changing pages.
    useEffect(() => {
        const unlisten = history.listen(checkOverflow);
        return () => {
            unlisten();
        }
    }, [history, checkOverflow]);

    return overflow;
}