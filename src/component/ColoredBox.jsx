import React from 'react';

import styles from './ColoredBox.module.css'

export default function ColoredBox(props) {
    return <div className={ styles.box }>
        <p className={ styles.heading }>{ props.heading }</p>
        <div>{ props.children }</div>
    </div>
}
