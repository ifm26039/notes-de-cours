import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

export default function StructProjet() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Après l'exécution de la commande ci-dessous, vous aurez un nouveau projet React prêt à être programmé.
            </p>
            <CodeBlock language="shell">{'npx create-next-app@latest'}</CodeBlock>
            <p>
                Vous devez par la suite répondre à quelques questions pour créer le projet:
            </p>
            <ul>
                <li>
                    Indiquer le nom du projet. Assurez-vous de mettre le nom en minuscule avec des tirets au lieu des 
                    espaces (Kebab Case).
                </li>
                <li>
                    Indiquer si vous voulez utiliser TypeScript. Dans le cours, je n'utiliserai pas TypeScript, mais je 
                    vous encourage à le regarder puisque c'est une technologie/langage de programmation très utilisé en 
                    ce moment.
                </li>
                <li>
                    Indiquer si vous voulez utiliser ESLint. Je vous recommande fortement de l'utiliser. C'est un outil 
                    d'évaluation du code Javascript, JSX ou TypeScript qui nous permet de trouver des erreurs avant de 
                    compiler l'application web. 
                </li>
            </ul>

            <p>
                Pour tester l'application, vous pouvez simplement lancer la commande suivante dans le terminal dans 
                le dossier du projet:
            </p>
            <CodeBlock language="shell">{'npm run dev'}</CodeBlock>
            <p>
                Ce projet comporte beaucoup d'élément dès sa création. Cette page vous résumera sommairement les 
                différentes sections et fichiers que vous y retrouverez.
            </p>
        </section>

        <section>
            <h2>Fichier <IC>package.json</IC></h2>
            <p>
                Le fichier <IC>package.json</IC> comprends les configurations du projet avec Node.js et NPM. Vous aurez
                peut-être à ajouter des configurations ici si vous ajoutez des librairies de code supplémentaire à
                votre projet.
            </p>
        </section>

        <section>
            <h2>Fichier <IC>next.config.js</IC></h2>
            <p>
                Fichier de configuration de Next.js. Vous aurrez probablement à ajouter quelques propriétés dans ce 
                fichier pour configurer certaines parties du développement de votre application web.
            </p>
        </section>

        <section>
            <h2>Dossier <IC>/public</IC></h2>
            <p>
                Le dossier <IC>/public</IC> contient les ressources qui ne seront pas compilé par Webpack et Babel.
                Dans ce dossier, vous trouverez les fichiers suivants:
            </p>
            <dl>
                <dt>Les icônes</dt>
                <dd>
                    <p>
                        Ce sont des fichiers, comme le <IC>favicon.ico</IC>, <IC>logo192.png</IC>, <IC>logo512.png</IC> ou 
                        tout autre fichier d'icône. Si vous changez les icônes, assurez-vous de les mettre dans les bons 
                        format d'image et dans les bonnes taille. Pour le fichier <IC>favicon.ico</IC>, assurez-vous qu'il 
                        contienne plusieurs dimensions de l'icône dans le fichier. 
                    </p>
                    <ColoredBox heading="Attention">
                        Simplement renommer l'extension d'un fichier d'image n'est pas bon! Il faut le convertir dans le
                        bon type avec un logiciel d'édition d'images. Plusieurs outils sont disponible en ligne pour
                        convertir les formats d'image ou pour générer des fichiers <IC>.ico</IC>.
                    </ColoredBox>
                </dd>

                <dt><IC>robot.txt</IC></dt>
                <dd>
                    Le fichier <IC>robot.txt</IC> indique aux robots d'indexation des engins de recherche, comme
                    Google, comment ceux-ci doivent s'y prendre pour analyser votre site web. À moins que vous voulez
                    que certaines pages ne soit pas indexées par les engins de recherche, vous n'avez pas besoin de
                    modifier ce fichier.
                </dd>

                <dt><IC>manifest.json</IC></dt>
                <dd>
                    Le fichier <IC>manifest.json</IC> donne des informations supplémentaires sur votre application
                    web. Il est pratique pour les applications web progressive que nous verrons plus tard. Finalement, 
                    les appareils Android peuvent l'utiliser pour définir les couleurs de l'interface du navigateur. 
                    Vous pouvez modifier le nom de l'application ainsi que ses couleurs quand vous serez rendu là dans 
                    votre développement.
                </dd>

                <dt>Fichiers d'images</dt>
                <dd>
                    <p>
                        Toutes les images de votre application web se trouveront aussi généralement 
                        dans le dossier <IC>public</IC>. Vous pouvez créer des sous-dossiers pour mieux organiser les images 
                        si vous en avez beaucoup.
                    </p>
                    <ColoredBox heading="Attention">
                        Next.js va essayer d'optimiser les images. On verra plus tard comment il fonctionne, mais sachez que 
                        les images dans ce dossier ne seront pas nécessairement celles utilisé par Next.js puisqu'il va les 
                        transformer pour augmenter ses performances.
                    </ColoredBox>
                </dd>

                <dt>Autres fichiers statiques</dt>
                <dd>
                    Tous fichiers statiques qui peuvent être téléchargé par l'utilisateur de votre page web pourront aussi 
                    trouver leur place dans le dossier <IC>public</IC>. Si vous voulez que certains de vos fichiers puisse être
                    téléchargeable dans votre site web en cliquant sur un bouton ou un lien, c'est ici qu'il faut les placer.
                </dd>
            </dl>
        </section>

        <section>
            <h2>Dossier <IC>/styles</IC></h2>
            <p>
                Le dossier <IC>/styles</IC> contient tous les fichiers de style de votre application. Vous n'êtes pas obligé de 
                mettre les fichiers de style ici, mais si le projet n'est pas trop gros, c'est un bon endroit pour les mettre. Il 
                y déjà quelques fichiers par défaut:
            </p>
            <dl>
                <dt><IC>global.css</IC></dt>
                <dd>
                    Le fichier CSS global qui sera utilisé par votre application entière. Ce fichier contient le CSS de très 
                    haut niveau de l'application. On y mets généralement les définitions CSS pour les polices de caractères ou 
                    tout autre CSS qui affecte toutes les pages de l'application.
                </dd>

                <dt>Autres fichiers <IC>.module.css</IC></dt>
                <dd>
                    Next.js nous permet d'utiliser les modules CSS, un concept que nous verrons dans les prochaines modules. 
                    Cela nous permet de mettre du CSS dans plusieurs fichiers sans risque de conflits. Vous aurez assurément à 
                    créer ce genre de fichier pour le style de vos applications web.
                </dd>
            </dl>
        </section>

        <section>
            <h2>Dossier <IC>/pages</IC></h2>
            <p>
                Le dossier <IC>/pages</IC> contient toutes les pages de votre application web. Next.js fait un énorme travail 
                d'optimisation des liens et des pages dans ce dossier. Assurez-vous de ne pas renommer le dossier. Pour chaque 
                nouvelle page que vous ajoutez à votre application, vous devez créer un fichier dans ce dossier. Il 
                y déjà quelques fichiers par défaut:
            </p>
            <dl>
                <dt><IC>_app.js</IC></dt>
                <dd>
                    C'est le fichier contenant la racine React de votre projet. On l'utilisera souvent si on veut se créer un gabarit 
                    de base pour chaque page de notre application web ou pour intégrer des fichiers global de CSS.
                </dd>

                <dt><IC>_document.js</IC></dt>
                <dd>
                    C'est le fichier contenant le HTML de base de votre application. On l'utilisera parfois pour modifier les attributs 
                    globals de la balise <IC>'&lt;html&gt;'</IC> et <IC>'&lt;body&gt;'</IC>.
                </dd>
                
                <dt><IC>index.js</IC></dt>
                <dd>
                    Ce fichier est la page d'accueil (page principale) de votre application. De plus, bien qu'il ait l'extension <IC>.js</IC>, c'est un 
                    fichier JSX. Je change d'ailleurs souvent son extension pour <IC>.jsx</IC>, mais ce n'est pas obligatoire. Nous écrirons le code de 
                    notre page d'accueil ici
                </dd>
            </dl>
        </section>

        <section>
            <h2>Création de dossiers supplémentaires</h2>
            <p>
                Le dossier <IC>/pages</IC> contient la majorité du code de votre application web. Toutefois, pour mieux diviser 
                votre projet, je vous recommande de créer 3 nouveaux sous-dossiers à la racine du projet:
            </p>
            <dl>
                <dt><IC>/components</IC></dt>
                <dd>
                    Pour contenir les composants de votre application. Nous verrons comment créer des composants
                    dans le prochain module.
                </dd>

                <dt><IC>/hooks</IC></dt>
                <dd>
                    Pour contenir certains comportements réutilisable de votre projet. Les hooks seront vu dans un module
                    plus tard.
                </dd>

                <dt><IC>/model</IC></dt>
                <dd>
                    Pour contenir les fichiers de données. Ces fichiers peuvent être des fichiers de base de données ou 
                    simplement des fichier de données en format texte, comme du JSON ou du XML.
                </dd>
            </dl>
        </section>
    </>
}