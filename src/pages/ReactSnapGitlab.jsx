import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const image =
`image: dseegers/puppeteer:latest
                
# ...`;

const install =
`stage: deploy

before_script:
  - npm install puppeteer

script:
  # ...`;

const conf =
`"reactSnap": {
    "puppeteerArgs": [
        "--no-sandbox", 
        "--disable-setuid-sandbox"
    ]
}`;

export default function ReactSnapGitlab() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Si vous voulez héberger gratuitement vos projets sur Gitlab ou Github, vous aurez quelques problèmes
                avec <IC>react&#8209;snap</IC>. En effet, le lancement de react-snap nécessite l'installation de Puppeteer, 
                une version de Chrome sans insterface graphique qui est exécuté uniquement en mémoire. Puppeteer peut être
                utilisé pour automatiser certains traitement, comme la conversion de page web en PDF, le testing de page web 
                ou encore, comme c'est le cas avec <IC>react&#8209;snap</IC>, de créer le pré-rendu d'une page web. Le
                problème de Puppeteer, c'est qu'il nécessite certaines configurations qui ne se retrouvent pas par défaut sur 
                les machines dans le cloud.
            </p>
            <p>
                Pour résoudre ce problème, nous devons faire 3 choses:
            </p>
            <ol>
                <li>Changer d'image Docker</li>
                <li>Installer Puppeteer</li>
                <li>Configurer Puppeteer dans <IC>react&#8209;snap</IC></li>
            </ol>
        </section>

        <section>
            <h2>Changer d'image Docker</h2>
            <p>
                Docker est un service en ligne permettant de définir des configurations de machines. Plusieurs services en 
                ligne nécessite utilise Docker pour démarrer des machines virtuelles dans le cloud. C'est d'ailleurs le cas
                de Gitlab et Github. Pour nous, ce changement est facile. Nous n'avons qu'à changer le nom de l'image utilisé 
                dans notre fichier d'intégration continue. Pour Gitlab, vous changez la propriété <IC>image</IC> du
                fichier <IC>.gitlab&#8209;ci.yml</IC> de la façon suivante:
            </p>
            <CodeBlock language="yaml">{image}</CodeBlock>
            <ColoredBox heading="Attention">
                <p>
                    L'image utilisé ici est mise à jour par un tierce partie en ligne. Ce tierce partie n'est pas tenu de mettre 
                    l'image à jour si de nouvelles versions de Node.js sont disponible ou si les configurations de base de
                    Puppeteer changent. J'ai d'ailleur eu beaucoup de difficultés à trouver une image avec les configurations de 
                    Puppeteer, mais qui supporte la dernière version de Node.js.
                </p>
                <p>
                    Bref, il est possible que vous ayez à trouver une nouvelle image Docker dans le futur.
                </p>
            </ColoredBox>
        </section>

        <section>
            <h2>Installer Puppeteer</h2>
            <p>
                L'image Docker ne vient pas par défaut avec Puppeteer. Elle vient uniquement avec les configurations
                nécessaires pour installer Puppeteer. Nous devrons donc ajouter un script d'installation à notre fichier
                d'automatisation. Pour Gitlab, vous pouvez ajouter la section <IC>before_script</IC> ci-dessous dans le 
                fichier <IC>.gitlab&#8209;ci.yml</IC>:
            </p>
            <CodeBlock language="yaml">{install}</CodeBlock>
        </section>

        <section>
            <h2>Configurer Puppeteer dans <IC>react&#8209;snap</IC></h2>
            <p>
                Si vous essayez de faire un commit et un push sur Gitlab ou Github, votre tâche va échouer à cause de 
                problèmes de sandboxing. Ceci est dû au fait que Puppeteer ajoute plusieurs sécurités pour empêcher des 
                scripts malicieux d'attaquer une machine utilisant Puppeteer. Si Puppeteer est utilisé pour parcourir des 
                sites web au hasard, ces sécurités sont primordiales. Toutefois, c'est complètement inutile si nous 
                l'utilisons pour parcourir notre propre site web, que nous avons codé nous-même et auquel nous avons
                confiance.
            </p>
            <p>
                Nous allons donc désactiver ces sécurités sur les Runner de Gitlab ou Github. Pour ce faire, vous n'avez
                qu'à modifier le fichier <IC>package.json</IC> et y ajouter la propriété <IC>reactSnap</IC>:
            </p>
            <CodeBlock language="json">{conf}</CodeBlock>
        </section>
    </>
}