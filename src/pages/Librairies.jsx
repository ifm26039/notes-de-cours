import React from 'react';
import IC from '../component/InlineCode';
import ColoredBox from '../component/ColoredBox'

export default function Librairies() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Cette page présentera une liste des librairies les plus importantes que nous utiliserons pour le
                développement de nos applications cette session. Plusieurs de ces librairies sont utilisé pour le
                développement de nos applications web et non pour son exécution dans le navigateur. Vous verrez que 
                l'architecture de développement d'une application web moderne est très complexe. Heureusement, la 
                configuration et l'installation sera faites presque automatiquement.
            </p>
        </section>
        
        <section>
            <h2>React</h2>
            <p>
                React est une librairie de Javascript client permettant de créer des interfaces graphiques web de
                façon efficace. En effet, React manipulera le <IC>DOM</IC> de vos pages web en s'assurant toujours
                d'être rapide et optimal. On peut l'utiliser pour développer des applications qui fonctionne sur une 
                seule page, ce qui améliore énormément la quantité d'appels aux serveurs web sur le réseau. 
            </p>
            <p>
                À la base, React crée ses pages et son code en Javascript uniquement, ce qui complexifie un peu son 
                utilisation lorqu'on doit faire le HTML pour définir l'interface graphique. Nous utiliserons donc un
                langage un peu différent du Javascript que nous appèlerons le JSX.
            </p>
        </section>

        <section>
            <h2>JSX</h2>
            <p>
                JSX est un langage de programmation très similaire au Javascript. En fait, il est pareil au
                Javascript, mais il y ajoute quelques fonctionnalités. C'est d'ailleurs d'où vient son nom: JSX veut
                dire Javascript Syntax Extension. La fonctionnalité principale ajouté par le JSX est celle de pouvoir
                utiliser du HTML directement dans le Javascript, ce qui est très pratique pour nous.
            </p>
            <p>
                Les navigateurs web ne sont toutefois pas capable de comprendre le JSX. Ils ne comprennent que le
                Javascript. Il faut donc convertir le langage JSX en Javascript avant de le lancer dans le navigateur
                web. Pour ce faire, nous utiliserons Babel, une librairie permettant de compiler des langages de
                programmation dans d'autres langages de programmation
            </p>
        </section>

        <section>
            <h2>Babel</h2>
            <p>
                Babel est un compilateur de code. Son but est généralement de compiler différents langages de
                programmation en Javacript. Dans notre cas, nous l'utiliserons pour compiler le JSX en Javascript pour 
                notre navigateur. Babel nous offre aussi d'autres utilités:
            </p>
            <ul>
                <li>
                    Il peut compiler le code dans du vieux Javascript pour être compatible avec les vieux navigateurs.
                    Cela nous permettra donc d'utiliser les dernières fonctionnalitées de Javascript tout en restant
                    compatible avec les dernières version d'Internet Explorer (ce qui n'est plus fait aujourd'hui).
                </li>
                <li>
                    Il peut optimiser le code Javascript là où c'est nécessaire. Ainsi, nous pouvons nous permettre de 
                    créer des variables avec des longs noms les identifiant bien et de clarifier notre code tout en
                    sachant que le code final lu par le navigateur sera optimisé automatiquement pour nous.
                </li>
            </ul>
        </section>

        <section>
            <h2>Webpack</h2>
            <p>
                Webpack est un bundler. C'est un utilitaire qui permet automatiquement de combiner, séparer ou ignorer
                certaines parties de votre code ou de vos fichiers pour optimiser les différentes parties de votre
                site web. Webpack sera entre autre utilisé pour pour intégré les librairies et les composants que nous 
                utiliserons dans nos site web. Il nous permettra d'ajouter du CSS de façon intéressante dans notre
                projet ainsi de faciliter la manipulation des images, des fichiers JSON et des autres ressources.
                C'est aussi Webpack qui se chargera de lancer Babel pour compiler notre code Javascript.
            </p>
            <ColoredBox heading="À noter">
                Webpack existe depuis de nombreuses années et plusieurs développeurs de plateforme web commence à le 
                trouver un peu lent pour de très grosses plateforme web. Plusieurs alternatives sont donc présentement 
                en développement. Il est très probable que Webpack soit remplacé par d'autres technologies dans les 
                prochaines années. 
            </ColoredBox>
            <p>
                Webpack peut facilement devenir un cauchemars de configuration. De plus, avec le branchement de Babel,
                JSX et React, il peut être facile de se perdre dans l'initialisation de nos projets. Heureusement, il
                existe des plateformes qui font le tout pour nous. C'est ce que fera Next.js.
            </p>
        </section>

        <section>
            <h2>Next.js</h2>
            <p>
                Next.js est un ensemble de librairies et de scripts nous permettant de facilement créer un projet utilisant 
                React. Cet ensemble de librairies, appelé un <IC>framework</IC> contient toutes les technologies mentionnées 
                ci-dessus ainsi qu'une panoplies d'autres outils et morceaux de code. Une simple commande nous permettra de 
                créer le projet avec une configuration de base plus que satisfaisante. Nous n'aurons donc pas à configurer le 
                JSX, Babel et Webpack. Tout sera fait automatiquement!
            </p>
        </section>
    </>
}