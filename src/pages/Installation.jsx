import React from 'react';
import IC from '../component/InlineCode';
import ColoredBox from '../component/ColoredBox';

export default function Instruction() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Ce cours portera principalement sur le développement d'application web cliente moderne utilisant une 
                plateforme de développement. Nous utiliserons la librairie React et la plateforme Next.js
                pour faire notre développement. Cette page vous présente la liste des logiciels et outils que nous
                devons télécharger et installer pour faire notre développement dans ce cours.
            </p>
        </section>

        <section>
            <h2>Node.js</h2>
            <p>
                Node.js est un environnement d'exécution du langage Javascript qui s'exécute dans un terminal sur
                votre ordinateur au lieu de dans un navigateur web. Nous utiliserons Node.js pour faciliter le
                développement client de notre application. Vous pouvez télécharger la dernière version ici:
            </p>
            <p>
                <a rel="noreferrer noopenner" target="_blank" href="https://nodejs.org/en/">Node.js</a>
            </p>
            <ColoredBox heading="Attention">
                Veuillez télécharger la version <IC>Current</IC> et non la version <IC>LTS</IC>. La version <IC>LTS</IC>,
                qui veut dire <IC>Long Term Support</IC> est une version plus vieille qui pourrait ne pas contenir les
                dernières fonctionnalitées de Node.js.
            </ColoredBox>
        </section>

        <section>
            <h2>Visual Studio Code</h2>
            <p>
                Visual Studio Code est un environnement de développement léger et flexible pouvant être utilisé avec
                n'importe quel langage de programmation. Il est toutefois spécialisé dans le développement
                d'application web. C'est présentement l'éditeur de code le plus utilisé sur le marché du travail. Vous
                pouvez télécharger la dernière version ici:
            </p>
            <p>
                <a rel="noreferrer noopenner" target="_blank" href="https://code.visualstudio.com/Download">Visual Studio Code</a>
            </p>
            <ColoredBox heading="Attention">
                Visual Studio Code n'est pas le même éditeur de code que Visual Studio. Même si les 2 sont développé
                par Microsoft, Visual Studio est un éditeur beaucoup plus volumineux et il n'est pas spécialisé pour
                le web, contrairement à Visual Studio Code. Téléchargez donc le bon outil!
            </ColoredBox>
        </section>

        <section>
            <h2>Git</h2>
            <p>
                Git est un outil de versionnement du code. Il permet facilement de garder plusieurs versions d'un code
                source et de changer entre elles. Si nous combinons ça avec un serveur Git, il est possible de 
                faciliter la collaboration entre les développeurs sur un même projet et d'automatiser certains
                processus de construction de l'application, comme son déploiement. Vous pouvez télécharger la dernière
                version ici:
            </p>
            <p>
                <a rel="noreferrer noopenner" target="_blank" href="https://git-scm.com/downloads">Git</a>
            </p>
        </section>
    </>
}