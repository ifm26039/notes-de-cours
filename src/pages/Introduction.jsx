import React from 'react';
import IC from '../component/InlineCode';

export default function Introduction() {
    return <>
        <section>
            <h2>Développement web client original</h2>
            <p>
                Le développement web original est celui que vous connaissez de base. Celui où l'on crée des fichiers
                HTML, CSS et Javascript. Celui où l'on doit tout lier ensemble dans le HTML avec des
                balises <IC>&lt;link&gt;</IC> et <IC>&lt;script&gt;</IC>. Ce développement web comporte plusieurs 
                avantages par rapport à d'autres langages de programmations:
            </p>
            <ul>
                <li>Si bien programmé, il est très performant dans un navigateur web.</li>
                <li>
                    Il possède un langage distinct spécialisé pour les 3 parties de son développement, soit la
                    structure, le visuel et le comportement.
                </li>
            </ul>
            <p>
                Malheureusement, avec ces avantages, il y a de nombreux inconvénients:
            </p>
            <ul>
                <li>
                    On ne peut jamais utiliser les dernières fonctionnalités pour des raisons de rétrocompatibilité
                    avec les vieux navigateurs.
                </li>
                <li>Si mal programmé, il est un cauchemar de performance et difficile à maintenir.</li>
                <li>
                    La gestion des balises <IC>&lt;link&gt;</IC> et <IC>&lt;script&gt;</IC> peut rapidement devenir un 
                    labyrinthe de prérequis si on divise bien notre code dans plusieurs fichiers.
                </li>
            </ul>
            <p>
                Le développement web moderne essai de palier à ces lacunes tout en gardant les avantages du
                développement web original.
            </p>
        </section>

        <section>
            <h2>Développement web client moderne</h2>
            <p>
                Le développement web moderne utilise des frameworks, donc des structures d'outils et de librairies, 
                pour permettre de faire du développement web utilisant les dernières fonctionnalités, mais tout en
                s'assurant qu'il reste compatible avec les vieux navigateurs. Il utilisera les dernières
                fonctionnalités du Javascript pour facilité le liage entre les fichiers. Il ira même jusqu'à
                utiliser des trucs un peu bizarre pour modulariser le CSS pour faciliter son organisation.
            </p>
            <p>
                Il existe plusieurs frameworks pour y arriver. Voici une liste de frameworks connus qui nous permette 
                de régler les problèmes ci-dessus:
            </p>
            <ul>
                <li>
                    <a rel="noreferrer noopenner" target="_blank" href="https://reactjs.org/">React</a>
                </li>
                <li>
                    <a rel="noreferrer noopenner" target="_blank" href="https://angular.io/">Angular</a>
                </li>
                <li>
                    <a rel="noreferrer noopenner" target="_blank" href="https://vuejs.org/">Vue.js</a>
                </li>
            </ul>
            <p>
                Dans ce cours, nous utiliserons React. Au moment de l'écriture de cette page, React est le framework
                web le plus utilisé par les programmeurs. Toutefois, les 3 frameworks ci-dessus sont très similaires.
                En apprendre un vous permettra d'utiliser les autres très facilement.
            </p>
        </section>
    </>
}