import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

export default function StructProjet() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Après l'exécution de la commande ci-dessous, vous aurez un nouveau projet React prêt à être programmé.
            </p>
            <CodeBlock language="shell">{'npx create-react-app nom-de-mon-application'}</CodeBlock>
            <p>
                Pour tester l'application, vous pouvez simplement lancer la commande suivante dans le terminal:
            </p>
            <CodeBlock language="shell">{'npm start'}</CodeBlock>
            <p>
                Ce projet comporte beaucoup d'élément dès sa création. Cette page vous résumera sommairement les 
                différentes sections et fichiers que vous y retrouverez.
            </p>
        </section>

        <section>
            <h2>Fichier <IC>package.json</IC></h2>
            <p>
                Le fichier <IC>package.json</IC> comprends les configurations du projet avec Node.js et NPM. Vous aurez
                peut-être à ajouter des configurations ici si vous ajoutez des librairies de code supplémentaire à
                votre projet.
            </p>
        </section>

        <section>
            <h2>Dossier <IC>/public</IC></h2>
            <p>
                Le dossier <IC>/public</IC> contient les ressources qui ne seront pas compilé par Webpack et Babel.
                Dans ce dossier, vous trouverez les fichiers suivants:
            </p>
            <dl>
                <dt>Les icônes</dt>
                <dd>
                    <p>
                        Ce sont les fichiers <IC>favicon.ico</IC>, <IC>logo192.png</IC> et <IC>logo512.png</IC>.
                        Si vous changez les icônes, assurez-vous de les mettre dans les bons format d'image et dans les 
                        bonnes taille. Pour le fichier <IC>favicon.ico</IC>, assurez-vous qu'il contienne plusieurs 
                        dimensions dans le fichier. 
                    </p>
                    <ColoredBox heading="Attention">
                        Simplement renommer l'extension d'un fichier d'image n'est pas bon! Il faut le convertir dans le
                        bon type avec un logiciel d'édition d'images. Plusieurs outils sont disponible en ligne pour
                        convertir les formats d'image ou pour générer des fichiers <IC>.ico</IC>.
                    </ColoredBox>
                </dd>

                <dt><IC>robot.txt</IC></dt>
                <dd>
                    Le fichier <IC>robot.txt</IC> indique aux robots d'indexation des engins de recherche, comme
                    Google, comment ceux-ci doivent s'y prendre pour analyser votre site web. À moins que vous voulez
                    que certaines pages ne soit pas indexées par les engins de recherche, vous n'avez pas besoin de
                    modifier ce fichier.
                </dd>

                <dt><IC>manifest.json</IC></dt>
                <dd>
                    Le fichier <IC>manifest.json</IC> donne des informations supplémentaires sur votre application
                    web. Il est pratique pour les applications web progressive que nous verrons plus tard. Ce fichier
                    est aussi utilisé pour offrir des liens plus intéressant dans d'autres applications, comme dans 
                    Teams, Facebook ou Twitter. Finalement, les appareils Android peuvent l'utiliser pour définir les 
                    couleurs de l'interface du navigateur. Vous pouvez modifier le nom de l'application ainsi que ses 
                    couleurs quand vous serez rendu là dans votre développement.
                </dd>

                <dt><IC>index.html</IC></dt>
                <dd>
                    <p>
                        Le fichier <IC>index.html</IC> contient le gabarit de base de votre site web. Contrairement à 
                        ce que l'on peut penser, on ne modifiera pas vraiment ce fichier. Si vous utilisez des polices de 
                        caractères spéciales, vous pouvez les ajouter dans le <IC>{'<head>'}</IC>. Vous pouvez aussi
                        changer le <IC>{'<meta name="description">'}</IC>, le <IC>{'<meta name="theme-color"'}</IC> ainsi
                        que le <IC>{'<title>'}</IC>.
                    </p>
                    <ColoredBox heading="Attention">
                        Vous ne devez en aucun cas ajouter des éléments ou modifier le contenu de la
                        balise <IC>{'<body>'}</IC>. React fonctionne en ajoutant du HTML à partir de code Javascript. 
                        Il s'attend donc à ce que la page par défaut soit presque vide.
                    </ColoredBox>
                </dd>
            </dl>
        </section>

        <section>
            <h2>Dossier <IC>/src</IC></h2>
            <p>
                Le dossier <IC>/src</IC> contient déjà quelques fichiers:
            </p>
            <dl>
                <dt><IC>index.css</IC></dt>
                <dd>
                    Le fichier <IC>index.css</IC>. Ce fichier contient le CSS de très haut niveau de l'application. On 
                    y mets généralement les définitions CSS pour les polices de caractères. Je trouve souvent ce 
                    fichier redondant et je copie son contenu dans le fichier <IC>App.css</IC>. Si vous le faites, 
                    vous pouvez supprimer ce fichier.
                </dd>

                <dt><IC>App.css</IC></dt>
                <dd>
                    Le fichier <IC>App.css</IC> contient le code CSS global de votre application. Il ne contiendra pas 
                    tout le code CSS, mais seulement celui qui peut être utilisé partout dans votre application.
                </dd>

                <dt><IC>index.js</IC></dt>
                <dd>
                    Ce fichier est le point d'entrée principal de votre application. Vous n'aurez pas vraiment besoin 
                    de modifier ce fichier, sauf si vous utiliser certaines librairies de code changeant la façon dont
                    React fait son rendu. Si vous avez supprimez le fichier <IC>index.css</IC>, vous devrez supprimer 
                    son <IC>import</IC> dans ce fichier. De plus, bien qu'il ait l'extension <IC>.js</IC>, c'est un 
                    fichier JSX. Je change d'ailleurs souvent son extension pour <IC>.jsx</IC>, mais ce n'est pas
                    obligatoire.
                </dd>

                <dt><IC>App.js</IC></dt>
                <dd>
                    C'est le fichier qui contient le code source de votre application. Nous le modifierons pour y 
                    ajouter des composants dans les prochains modules. Pour l'instant, vous pouvez vous amuser avec 
                    celui-ci en modifiant le HTML et voir le résultat dans le navigateur. Ce fichier aussi est un 
                    fichier JSX. Vous pouvez donc aussi changer son extension pour <IC>.jsx</IC>.
                </dd>

                <dt>Fichiers de test et de WebVitals</dt>
                <dd>
                    Create React App vient avec un système de test pour automatiser le testing de votre application. 
                    De plus, il possède des utilitaires pour tester ses performances. Ce genre de programmation est 
                    très pratique sur les gros systèmes informatiques puisqu'ils permettent d'accélérer la recherche 
                    de bogues, d'éviter de faire des maintenances fautives et de s'assurer de la qualité de 
                    l'application. Ceci étant dit, nous ne verrons pas comment les utiliser dans ce cours. Vous pouvez
                    toutefois faire un peut de lecture sur ce sujet pour vous renseigner.
                </dd>

                <dt>Autres fichiers</dt>
                <dd>
                    Si votre dossier <IC>/src</IC> contient d'autres fichiers, ceux-ci sont généralement utilisé à des 
                    fins de démonstration. Bref, une fois votre application de base créé, vous pourrez les supprimer.
                </dd>
            </dl>
        </section>

        <section>
            <h2>Création de dossiers supplémentaires</h2>
            <p>
                Le dossier <IC>/src</IC> contient le code de votre application web. Pour mieux diviser votre projet,
                je vous recommande d'y créer 3 sous-dossier:
            </p>
            <dl>
                <dt><IC>/src/components</IC></dt>
                <dd>
                    Pour contenir les composants de votre application. Nous verrons comment créer des composants
                    dans le prochain module.
                </dd>

                <dt><IC>/src/pages</IC></dt>
                <dd>
                    Pour contenir les différentes "pages" de votre application. Les pages seront aussi des
                    composants, mais il est plus facile des les séparer.
                </dd>

                <dt><IC>/src/resources</IC></dt>
                <dd>
                    Pour contenir les autres ressources utilisé par votre site web. Vous pouvez y mettre des
                    fichiers d'images, des vidéos, des fichiers JSON, des PDFs ou tout autre fichier que vous 
                    voulez rendre disponible à l'utilisateur de votre site web.
                </dd>
            </dl>
        </section>
    </>
}