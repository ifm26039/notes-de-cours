export function formatUnsignedInt(number, size) {
    return ('0'.repeat(size - 1) + (number + 1)).slice(-size);
}

export function debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args)
        }, timeout);
    }
}

const Utils = {};
export default Utils;
