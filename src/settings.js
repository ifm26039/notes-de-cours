// Syntax highlighter imports for styles
import vs from 'react-syntax-highlighter/dist/esm/styles/prism/vs';
import vsdark from 'react-syntax-highlighter/dist/esm/styles/prism/vsc-dark-plus';

// Settings object
let settings = {
    naming: {
        title: 'Programmation web avancé',
        sections: 'Module'
    },
    themes: {
        default: 'light',
        properties: {
            light: {
                bgColor: '#fff',
                bgAccentColor: '#cc0e00',
                bgAccentGradientColor: '#ff391f',
                bgInteractiveColor: '#6e6260',
                bgInteractiveAccentColor: '#ffcec2',
                bgHighlight: '#cfcccc',
                bgHighlightAccent: '#ffdac7',
                borderInteractiveColor: '#c4b3b1',
                borderSeparatorColor: '#c3c3c3',
                textColor: '#222',
                textInvertedColor: '#fff',
                textAsideColor: '#222',
                textSwitcherColor: '#cc0e00',
                syntaxHighlightStyle: 'vs'
            },
            dark: {
                bgColor: '#36352e',
                bgAccentColor: '#ff8d0a',
                bgAccentGradientColor: '#ffa60d',
                bgHighlight: '#87867c',
                bgHighlightAccent: '#f07c00',
                bgInteractiveColor: '#f9cf90',
                bgInteractiveAccentColor: '#f9cf90',
                borderInteractiveColor: '#ffa217',
                borderSeparatorColor: '#c3c3c3',
                textColor: '#fff',
                textInvertedColor: '#fff',
                textAsideColor: '#f9cf90',
                textSwitcherColor: '#ffa60d',
                syntaxHighlightStyle: 'vsdark'
            }
        }
    },
    syntaxHiglight: {
        languages: {
            html: { name: 'HTML', parser: 'markup' },
            css: { name: 'CSS', parser: 'css' },
            js: { name: 'Javascript', parser: 'javascript' },
            jsx: { name: 'JSX', parser: 'jsx' },
            shell: { name: 'Terminal', parser: 'powershell' },
            yaml: { name: 'YAML', parser: 'yaml' },
            json: { name: 'JSON', parser: 'json' }
        },
        themes: {
            vs: vs,
            vsdark: vsdark
        }
    }
}

export default settings;
export let naming = settings.naming;
export let themes = settings.themes;
export let syntaxHiglight = settings.syntaxHiglight;