import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { HelmetProvider } from 'react-helmet-async';
import { ThemeContextProvider } from './ThemeContext';
import ScrollToTop from './component/ScrollToTop';
import Header from './component/Header';
import Aside from './component/Aside';
import Main from './component/Main';
import Footer from './component/Footer';

import './App.css';

export default function App() {    
    return <React.StrictMode>
        <BrowserRouter basename={process.env.PUBLIC_URL}>
            <HelmetProvider>
                <ThemeContextProvider>
                    <ScrollToTop />
                    <Header />
                    <Aside />
                    <Main />
                    <Footer />
                </ThemeContextProvider>
            </HelmetProvider>
        </BrowserRouter>
    </React.StrictMode>
}
